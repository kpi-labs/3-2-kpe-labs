const Joi = require('@hapi/joi');

const JwtTokenConfig = Joi.object({
  secret: Joi.string(),
  ttl: Joi.number()
    .positive(),
  algorithm: Joi.string()
    .default('HS256')
    .optional()
});

module.exports = Joi.object({
  isDev: Joi.boolean()
    .default(false)
    .optional(),
  log4js: Joi.object({
    appenders: Joi.object(),
    categories: Joi.object()
  }),
  env: Joi.string()
    .valid('development', 'production', 'test')
    .default(process.env.NODE_ENV || 'development')
    .optional(),
  app: Joi.object({
    port: Joi.number(),
    host: Joi.string()
      .optional()
      .default('localhost'),
  }),
  knex: Joi.object({
    host: Joi.string(),
    port: Joi.number(),
    user: Joi.string(),
    database: Joi.string(),
    password: Joi.string(),
    dialect: Joi.string()
      .valid('pg', 'mysql', 'mysql2', 'sqlite3', 'mssql'),
    pool: Joi.object({
      min: Joi.number()
        .optional()
        .min(1),
      max: Joi.number()
        .optional()
        .min(1)
    })
      .optional()
  }),
  redis: Joi.object({
    host: Joi.string(),
    port: Joi.number()
      .default(6379)
      .optional(),
    auth: Joi.string()
      .optional()
  })
    .optional(),
  jwt: Joi.object({
    access: JwtTokenConfig,
    refresh: JwtTokenConfig,
  }),
});
