const { loadConfig } = require('@zulus/config');
const configSchema = require('./config.server.schema');
const log4js = require('log4js');

const config = loadConfig(__dirname, configSchema, null, 'config.server.#env');
log4js.configure(config.log4js);

module.exports = config;
