const path = require('path');

module.exports = {
  isDev: true,
  knex: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    database: process.env.DB_NAME,
    password: process.env.DB_PSW,
    dialect: 'pg',
    pool: {
      min: 5,
      max: 50
    }
  },
  app: {
    port: 3000,
    host: 'localhost'
  },
  redis: {
    host: 'localhost',
    port: 6379
  },
  log4js: {
    appenders: {
      console: {
        type: 'console',
      }
    },
    categories: {
      default: {
        appenders: [
          'console'
        ],
        level: 'off'
      },
      http: {
        appenders: [
          'console'
        ],
        level: 'off'
      }
    }
  },

  jwt: {
    access: {
      secret: 'abc',
      ttl: 60
    },
    refresh: {
      secret: '???',
      ttl: 160
    },
  },
  oauth2CredsPaths: {
    google: path.join(__dirname, '../../google-creds.json')
  }
};
