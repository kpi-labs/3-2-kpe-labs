const path = require('path');

module.exports = {
  isDev: true,
  knex: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    database: process.env.DB_NAME,
    password: process.env.DB_PSW,
    dialect: 'pg',
    pool: {
      min: 5,
      max: 50
    }
  },
  app: {
    port: 3000,
    host: 'localhost'
  },
  redis: {
    host: 'localhost',
    port: 6379
  },
  log4js: {
    appenders: {
      file: {
        type: 'dateFile',
        pattern: 'yyyy-MM-dd',
        filename: path.join(process.cwd(), '/logs/server.log'),
        compress: true,
        alwaysIncludePattern: true,
        daysToKeep: 31,
        keepFileExt: true,
        numBackups: 3
      },
      console: {
        type: 'console',
        layout: {
          type: 'coloured'
        }
      },
      pureConsole: {
        type: 'stdout',
        layout: {
          type: 'dummy'
        }
      }
    },
    categories: {
      default: {
        appenders: [
          'file',
          'console'
        ],
        level: 'DEBUG'
      },
      http: {
        appenders: [
          'pureConsole'
        ],
        level: 'DEBUG'
      }
    }
  },
  jwt: {
    access: {
      secret: 'abc',
      ttl: 60
    },
    refresh: {
      secret: '???',
      ttl: 160
    },
  },
};
