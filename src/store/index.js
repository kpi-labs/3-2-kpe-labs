module.exports = {
  Users: require('./Users'),
  CriminalArticleModel: require('./CriminalArticles'),
  Pccos: require('./Pccos'),
  PccosUpdatesLogs: require('./PccosUpdatesLogs'),
};
