const { Model } = require('objection');
const constants = require('../constants');
const helpers = require('../helpers');
const { pick } = require('lodash');

class Users extends Model {
  static get idColumn() {
    return 'id';
  }

  static get tableName() {
    return 'users';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [
        'email',
        'password'
      ],
      properties: {
        id: { type: 'number' },
        password: { type: 'string' },
        role: { type: 'string' },
        createdAt: { type: 'string' },
      }
    };
  }

  async $beforeInsert(queryContext) {
    this.password = await helpers.passwords.encryptPassword(this.password, constants.passwordSaltRounds);
    await super.$beforeInsert(queryContext);
  }

  async $beforeUpdate(queryContext) {
    this.password = await helpers.passwords.encryptPassword(this.password, constants.passwordSaltRounds);
    await super.$beforeUpdate(queryContext);
  }

  static async isEmailTaken(email, { trx } = {}) {
    const result = await Users.query(trx)
      .where({ email })
      .count();
    return !result[0].count;
  }

  static findByEmail(email) {
    return Users.query()
      .findOne({ email });
  }

  toPrivateJson() {
    return pick(this, ['id', 'email', 'role', 'createdAt']);
  }

  static get publicFields() {
    return ['id', 'email'];
  }

  toPublicJson() {
    return pick(this, Users.publicFields);
  }
}

module.exports = Users;
