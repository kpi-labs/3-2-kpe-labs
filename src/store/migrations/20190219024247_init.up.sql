CREATE TYPE USER_ROLE AS ENUM ('administrator', 'recorder');

CREATE TYPE LOG_TYPE AS ENUM ('UPDATE_PCCO', 'DEACTIVATE_PCCO', 'ACTIVATE_PCCO', 'CREATE_PCCO');

CREATE TABLE IF NOT EXISTS users
(
    id         SERIAL                  NOT NULL
        CONSTRAINT users_pk
            PRIMARY KEY,
    email      VARCHAR(256)            NOT NULL,
    password   VARCHAR(64)             NOT NULL,
    created_at TIMESTAMP DEFAULT now() NOT NULL,
    role       USER_ROLE               NOT NULL
);


CREATE UNIQUE INDEX IF NOT EXISTS users_email_uindex
    ON users (email);

CREATE UNIQUE INDEX IF NOT EXISTS users_id_uindex
    ON users (id);

CREATE TABLE IF NOT EXISTS criminal_articles
(
    id      SERIAL NOT NULL
        CONSTRAINT criminal_articles_pk
            PRIMARY KEY,
    article TEXT   NOT NULL
);

CREATE TABLE IF NOT EXISTS pccos
(
    id                                      SERIAL               NOT NULL
        CONSTRAINT individual_pccos_pk
            PRIMARY KEY,
    judicature_decision_date                DATE                 NOT NULL,
    judicature_decision_number              VARCHAR(64),
    judicature_name                         TEXT                 NOT NULL,
    litigation_number                       VARCHAR(64)          NOT NULL,
    judicature_decision_applying_date       DATE                 NOT NULL,
    criminal_action_type                    TEXT                 NOT NULL,
    criminal_code_article                   INTEGER              NOT NULL
        CONSTRAINT pccos_criminal_articles_id_fk
            REFERENCES criminal_articles,
    offence_description                     TEXT                 NOT NULL,
    first_name                              VARCHAR(64),
    last_name                               VARCHAR(64),
    surname                                 VARCHAR(64),
    personal_code                           VARCHAR(8)           NOT NULL,
    work_position                           TEXT,
    work_place                              TEXT,
    passport_series                         VARCHAR(2),
    passport_code                           VARCHAR(6),
    dob                                     DATE,
    place_of_birth                          TEXT,
    residence                               TEXT,
    imposition_disciplinary_action_details  VARCHAR(128),
    offence_method                          TEXT                 NOT NULL,
    disciplinary_action_cancellation_date   DATE,
    disciplinary_action_cancellation_reason TEXT,
    passport_issuing_authority              TEXT,
    conviction_repayment_date               DATE,
    conviction_repayment_reason             TEXT,
    disciplinary_punishment_type            TEXT,
    is_active                               BOOLEAN DEFAULT TRUE NOT NULL,
    full_title                              TEXT,
    short_title                             VARCHAR(64),
    is_ukr_residence                        BOOLEAN,
    legal_form                              TEXT,
    offence_location                        TEXT,
    cancellation_date                       DATE,
    cancellation_reason                     TEXT,
    is_personal                             BOOLEAN              NOT NULL
);

COMMENT ON COLUMN pccos.judicature_decision_date IS 'дата винесення вироку судом';

COMMENT ON COLUMN pccos.judicature_decision_number IS 'номер вироку суду';

COMMENT ON COLUMN pccos.judicature_name IS 'назва суду, що виніс вирок';

COMMENT ON COLUMN pccos.litigation_number IS 'номер судової справи';

COMMENT ON COLUMN pccos.judicature_decision_applying_date IS 'дата набрання законної сили судовим рішенням про притягнення особи до відповідальності за корупційне або пов’язане з корупцією правопорушення;';

COMMENT ON COLUMN pccos.criminal_action_type IS 'вид покарання (стягнення), суть задоволення позовних вимог;';

COMMENT ON COLUMN pccos.criminal_code_article IS 'стаття (частина статті) Кримінального кодексу України або Кодексу України про адміністративні правопорушення, відповідно до якої особу притягнуто до відповідальності;
';

COMMENT ON COLUMN pccos.offence_description IS 'склад корупційного або пов’язаного з корупцією правопорушення;';

COMMENT ON COLUMN pccos.first_name IS 'ім''я';

COMMENT ON COLUMN pccos.last_name IS 'прізвище';

COMMENT ON COLUMN pccos.surname IS 'по-батькові';

COMMENT ON COLUMN pccos.personal_code IS 'реєстраційний номер облікової картки платника податків (за наявності);';

COMMENT ON COLUMN pccos.work_position IS 'посада на час вчинення корупційного або пов’язаного з корупцією правопорушення;
';

COMMENT ON COLUMN pccos.work_place IS 'місце роботи на час вчинення корупційного або пов’язаного з корупцією правопорушення;
';

COMMENT ON COLUMN pccos.passport_series IS 'серія паспорта';

COMMENT ON COLUMN pccos.passport_code IS 'номер паспорта';

COMMENT ON COLUMN pccos.dob IS 'дата народження';

COMMENT ON COLUMN pccos.place_of_birth IS 'місце народження';

COMMENT ON COLUMN pccos.residence IS 'місце проживання (для іноземців, осіб без громадянства - місце проживання за межами України);';

COMMENT ON COLUMN pccos.imposition_disciplinary_action_details IS 'реквізити розпорядчого документа про накладення дисциплінарного стягнення;';

COMMENT ON COLUMN pccos.offence_method IS 'спосіб вчинення дисциплінарного проступку;';

COMMENT ON COLUMN pccos.disciplinary_action_cancellation_date IS 'дата скасування розпорядчого документа про накладення дисциплінарного стягнення.';

COMMENT ON COLUMN pccos.disciplinary_action_cancellation_reason IS 'підстава для скасування розпорядчого документа про накладення дисциплінарного стягнення.';

COMMENT ON COLUMN pccos.passport_issuing_authority IS 'ким виданий паспорт';

COMMENT ON COLUMN pccos.conviction_repayment_date IS 'дата зняття, погашення судимості;';

COMMENT ON COLUMN pccos.conviction_repayment_reason IS 'підстава для погашення судимості;
';

COMMENT ON COLUMN pccos.disciplinary_punishment_type IS 'вид дисциплінарного стягнення;';

COMMENT ON COLUMN pccos.full_title IS 'найменування юридичної особи (повне);';

COMMENT ON COLUMN pccos.short_title IS 'найменування юридичної особи (скорочене);';

COMMENT ON COLUMN pccos.is_ukr_residence IS 'належність до резидентів України;';

COMMENT ON COLUMN pccos.legal_form IS 'організаційно-правова форма юридичної особи;';

COMMENT ON COLUMN pccos.offence_location IS 'місцезнаходження юридичної особи на час скоєння правопорушення;';

COMMENT ON COLUMN pccos.cancellation_date IS 'дата звільнення юридичної особи від застосування заходів кримінально-правового характеру.';

COMMENT ON COLUMN pccos.cancellation_reason IS 'підстава для звільнення юридичної особи від застосування заходів кримінально-правового характеру.';

COMMENT ON COLUMN pccos.is_personal IS 'це фізична особа';

CREATE UNIQUE INDEX IF NOT EXISTS individual_pccos_id_uindex
    ON pccos (id);

CREATE UNIQUE INDEX IF NOT EXISTS individual_pccos_litigation_number_uindex
    ON pccos (litigation_number);

CREATE UNIQUE INDEX IF NOT EXISTS individual_pccos_personal_code_uindex
    ON pccos (personal_code);

CREATE TABLE IF NOT EXISTS pccos_updates_logs
(
    id                 BIGSERIAL               NOT NULL
        CONSTRAINT action_logs_pk
            PRIMARY KEY,
    user_id            INTEGER                 NOT NULL
        CONSTRAINT action_logs_users_id_fk
            REFERENCES users,
    created_at         TIMESTAMP DEFAULT now() NOT NULL,
    affected_record_id INTEGER
        CONSTRAINT action_logs_pccos_id_fk
            REFERENCES pccos,
    old_values         JSON,
    new_values         JSON,
    type               LOG_TYPE,
    updation_reason    TEXT                    NOT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS action_logs_id_uindex
    ON pccos_updates_logs (id);

CREATE UNIQUE INDEX IF NOT EXISTS criminal_articles_article_uindex
    ON criminal_articles (article);

CREATE UNIQUE INDEX IF NOT EXISTS criminal_articles_id_uindex
    ON criminal_articles (id);

