const { Model } = require('objection');
const { pick } = require('lodash');

class Pccos extends Model {
  static get idColumn() {
    return 'id';
  }

  static get tableName() {
    return 'pccos';
  }

  static get allowedToModificationFields() {
    return [
      'judicatureDecisionDate',
      'judicatureDecisionNumber',
      'judicatureName',
      'litigationNumber',
      'judicatureDecisionApplyingDate',
      'criminalActionType',
      'criminalCodeArticle',
      'offenceDescription',
      'firstName',
      'lastName',
      'surname',
      'personalCode',
      'workPosition',
      'workPlace',
      'passportSeries',
      'passportCode',
      'dob',
      'placeOfBirth',
      'residence',
      'impositionDisciplinaryActionDetails',
      'offenceMethod',
      'disciplinaryActionCancellationDate',
      'disciplinaryActionCancellationReason',
      'passportIssuingAuthority',
      'convictionRepaymentDate',
      'convictionRepaymentReason',
      'disciplinaryPunishmentType',
      'cancellationDate',
      'cancellationReason',
    ];
  }

  static get jsonSchema() {
    return {
      type: 'object',

      properties: {
        id: { type: 'integer' },

        judicatureDecisionDate: { type: 'date' },

        judicatureDecisionNumber: { type: 'string' },

        judicatureName: { type: 'string' },

        litigationNumber: { type: 'string' },

        judicatureDecisionApplyingDate: { type: 'date' },

        criminalActionType: { type: 'string' },

        criminalCodeArticle: { type: 'integer' },

        offenceDescription: { type: 'string' },

        firstName: { type: 'string' },

        lastName: { type: 'string' },

        surname: { type: 'string' },

        personalCode: { type: 'string' },

        workPosition: { type: 'string' },

        workPlace: { type: 'string' },

        passportSeries: { type: 'string' },

        passportCode: { type: 'string' },

        dob: { type: 'date' },

        placeOfBirth: { type: 'string' },

        residence: { type: 'string' },

        impositionDisciplinaryActionDetails: { type: 'string' },

        offenceMethod: { type: 'string' },

        disciplinaryActionCancellationDate: { type: 'string' },

        disciplinaryActionCancellationReason: { type: 'string' },

        passportIssuingAuthority: { type: 'string' },

        convictionRepaymentDate: { type: 'string' },

        convictionRepaymentReason: { type: 'string' },

        disciplinaryPunishmentType: { type: 'string' },

        isActive: { type: 'boolean' },

        cancellationDate: { type: 'string' },

        cancellationReason: { type: 'string' },

      }
    };
  }

  static get publicFields() {
    return [
      'id',

      'judicatureDecisionDate',

      'judicatureDecisionNumber',

      'judicatureName',

      'litigationNumber',

      'judicatureDecisionApplyingDate',

      'criminalActionType',

      'criminalCodeArticle',

      'offenceDescription',

      'firstName',

      'lastName',

      'surname',

      'personalCode',

      'workPosition',

      'workPlace',

      'dob',

      'placeOfBirth',

      'residence',

      'impositionDisciplinaryActionDetails',

      'offenceMethod',

    ];
  }

  toPrivateJson() {
    return pick(this,
      Object.keys(Pccos.jsonSchema.properties));
  }

  toPublicJson() {
    return pick(this,
      Pccos.publicFields);
  }
}

module.exports = Pccos;
