const { Model } = require('objection');
const { pick } = require('lodash');

class PccosUpdatesLogs extends Model {
  static get idColumn() {
    return 'id';
  }

  static get tableName() {
    return 'pccos_updates_logs';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        userId: { type: 'integer' },
        affectedRecordId: { type: 'integer' },
        oldValues: { type: 'object' },
        newValues: { type: 'object' },
        type: { type: 'string' },
        updationReason: { type: 'string' },
        createdAt: { type: 'string' }
      }
    };
  }

  static get publicFields() {
    return ['id', 'userId', 'affectedRecordId', 'updationReason', 'type', 'createdAt','oldValues','newValues'];
  }

  toPrivateJson() {
    return pick(this, Object.keys(PccosUpdatesLogs.jsonSchema.properties));
  }

  toPublicJson() {
    return pick(this, PccosUpdatesLogs.publicFields);
  }
}

module.exports = PccosUpdatesLogs;
