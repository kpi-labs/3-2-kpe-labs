const { Model } = require('objection');
const { pick } = require('lodash');

class CriminalArticles extends Model {
  static get idColumn() {
    return 'id';
  }

  static get tableName() {
    return 'criminal_articles';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'number' },
        title: { type: 'string' },
      }
    };
  }

  toPrivateJson() {
    return this.toPublicJson();
  }

  toPublicJson() {
    return pick(this, ['id', 'title']);
  }
}

module.exports = CriminalArticles;
