const config = require('../config/server');
const knex = require('./helpers/connections').getKnex(config.knex);
const path = require('path');

knex.migrate
  .latest({
    directory: path.join(__dirname, './store/migrations'),
  })
  .then(() => {
    knex.destroy();
  });
