module.exports = {
  passwordSaltRounds: 10,
  userRoles: require('./user-roles'),
  pccosUpdatesTypes: require('./pccos-updates-types')
};
