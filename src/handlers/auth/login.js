const { auth } = require('../../helpers');

const login = async (ctx) => {
  const { user } = ctx.state;
  const tokens = await auth.getTokensForUser(user, ctx.config.jwt);
  ctx.res.ok({
    data: {
      user: user.toPrivateJson(),
      tokens
    }
  });
};

module.exports = login;
