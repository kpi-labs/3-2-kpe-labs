async function get(ctx, next) {
  const { user } = ctx.request;
  ctx.res.ok({ data: user.toPrivateJson() });
  await next();
}

module.exports = get;
