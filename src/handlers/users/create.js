const { Joi } = require('../../helpers/JoiObjects/validation-objects');
const { userRoles } = require('../../constants');
const logger = require('log4js')
  .getLogger('create');

async function create(ctx, next) {
  const body = ctx.request.body;
  if (await ctx.store.Users.isEmailTaken(body.email)) {
    return ctx.res.badRequest({ message: 'This email is already taken' });
  }
  try {
    await ctx.store.Users.query()
      .insert(body);
    return ctx.res.ok();
  } catch (e) {
    logger.error(e);
    return ctx.res.error(e);
  }
  await next();
}

create.schema = {
  body: Joi.object({
    email: Joi.string()
      .trim()
      .email(),
    password: Joi.password()
      .isPassword(),
    role: Joi.string()
      .valid(userRoles)
  })
};

module.exports = create;
