const { Joi } = require('../../helpers/JoiObjects/validation-objects');
const { userRoles } = require('../../constants');

async function update(ctx, next) {
  const { user } = ctx.request;
  await user.$query()
    .update(ctx.request.body);
  ctx.res.ok();
  await next();
}

update.schema = {
  params: Joi.object({
    userId: Joi.number()
      .min(0)
  }),
  body: Joi.object({
    email: Joi.string()
      .trim()
      .email()
      .optional(),
    password: Joi.password()
      .isPassword()
      .optional(),
    role: Joi.string()
      .valid(userRoles)
      .optional()
  })
};

module.exports = update;
