const { Joi } = require('../../helpers/JoiObjects/validation-objects');
const _ = require('lodash');
const logger = require('log4js')
  .getLogger('list-users');
const { userRoles } = require('../../constants');

const POSSIBLE_QUERIED_COLUMNS = ['email', 'createdAt', 'role'];

function getWhereQuery(reqQuery, Users) {
  let q = Users.query();
  const queryParams = _.pick(reqQuery, POSSIBLE_QUERIED_COLUMNS);
  for (const column in queryParams) {
    if (typeof queryParams[column] === 'string') {
      q = q.andWhere(column, 'ilike', `%${queryParams[column]}%`);
    } else {
      q = q.andWhere(column, '=', queryParams[column]);
    }
  }
  return q;
}

async function list(ctx, next) {
  const { store: { Users } } = ctx;
  try {
    const query = getWhereQuery(ctx.request.query, Users);
    const [users, { total }] = await Promise.all([
      query.clone()
        .offset(ctx.request.query.skip)
        .limit(ctx.request.query.limit)
        .select(Users.publicFields),
      query.count('id as total')
        .first()]);
    ctx.res.ok({
      data: users.map(r => r.toPublicJson()),
      meta: {
        total: total,
        limit: ctx.request.query.limit,
        skip: ctx.request.query.skip,
      }
    });
  } catch (e) {
    logger.error(e);
    return ctx.res.error(e);
  }
  await next();
}

list.schema = {
  query: Joi.object({
    email: Joi.string()
      .trim()
      .min(2)
      .optional(),
    role: Joi.string()
      .optional()
      .valid(userRoles),
    skip: Joi.number()
      .min(0)
      .optional()
      .default(0),
    limit: Joi.number()
      .min(1)
      .optional()
      .default(20)
  })
};

module.exports = list;
