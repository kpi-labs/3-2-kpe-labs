const { Joi } = require('../../helpers/JoiObjects/validation-objects');
const _ = require('lodash');
const logger = require('log4js')
  .getLogger('load-user');

async function load(ctx, next) {
  const { store: { Users } } = ctx;
  try {
    const user = await Users.query()
      .findById(ctx.request.params.userId);
    if (!user) {
      return ctx.res.notFound();
    } else {
      ctx.request.user = user;
    }
  } catch (e) {
    logger.error(e);
    return ctx.res.error(e);
  }
  await next();
}

load.schema = {
  params: Joi.object({
    userId: Joi.number()
      .min(0),
  }),
};

module.exports = load;
