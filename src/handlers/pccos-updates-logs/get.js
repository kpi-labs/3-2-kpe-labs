async function get(ctx, next) {
  const { pccoUpdateLog } = ctx.request;
  ctx.res.ok({ data: pccoUpdateLog.toPrivateJson() });
  await next();
}

module.exports = get;
