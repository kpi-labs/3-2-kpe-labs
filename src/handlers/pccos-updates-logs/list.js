const { Joi } = require('../../helpers/JoiObjects/validation-objects');
const _ = require('lodash');
const logger = require('log4js')
  .getLogger('create-pccos');

const POSSIBLE_QUERIED_COLUMNS = ['userId', 'affectedRecordId'];

function getWhereQuery(reqQuery, PccosUpdatesLogs) {
  let q = PccosUpdatesLogs.query();
  const queryParams = _.pick(reqQuery, POSSIBLE_QUERIED_COLUMNS);
  for (const column in queryParams) {
    if (column === 'affectedRecordId') {
      q = q.andWhere(column, '=', queryParams[column]);
    } else if (typeof queryParams[column] === 'string') {
      q = q.andWhere(column, 'ilike', `${queryParams[column]}%`);
    } else {
      q = q.andWhere(column, '=', queryParams[column]);
    }
  }
  return q;
}

async function list(ctx, next) {
  const { store: { PccosUpdatesLogs } } = ctx;
  try {
    const query = getWhereQuery(ctx.request.query, PccosUpdatesLogs);
    const [pccosUpdatesLogs, { total }] = await Promise.all([
      query.clone()
        .offset(ctx.request.query.skip)
        .limit(ctx.request.query.limit)
        .select(PccosUpdatesLogs.publicFields),
      query.count('id as total')
        .first()]);
    ctx.res.ok({
      data: pccosUpdatesLogs.map(r => r.toPublicJson()),
      meta: {
        total: total,
        limit: ctx.request.query.limit,
        skip: ctx.request.query.skip,
      }
    });
  } catch (e) {
    logger.error(e);
    return ctx.res.error(e);
  }
  await next();
}

list.schema = {
  query: Joi.object({
    userId: Joi.number()
      .min(0)
      .optional(),
    affectedRecordId: Joi.number()
      .min(2)
      .optional(),
    skip: Joi.number()
      .min(0)
      .optional()
      .default(0),
    limit: Joi.number()
      .min(1)
      .optional()
      .default(20)
  })
};

module.exports = list;
