const { Joi } = require('../../helpers/JoiObjects/validation-objects');
const _ = require('lodash');
const logger = require('log4js')
  .getLogger('load-personal-pccos');

async function load(ctx, next) {
  const { store: { PccosUpdatesLogs } } = ctx;
  try {
    const pccoUpdateLog = await PccosUpdatesLogs.query()
      .findById(ctx.request.params.logId);
    if (!pccoUpdateLog) {
      return ctx.res.notFound();
    } else {
      ctx.request.pccoUpdateLog = pccoUpdateLog;
    }
  } catch (e) {
    logger.error(e);
    return ctx.res.error(e);
  }
  await next();
}

load.schema = {
  params: Joi.object({
    logId: Joi.number()
      .min(0),
  }),
};

module.exports = load;
