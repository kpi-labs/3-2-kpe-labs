const { Joi } = require('../../helpers/JoiObjects/validation-objects');
const { transaction } = require('objection');
const { pccosUpdatesTypes: { UPDATE_PCCO } } = require('../../constants');
const logger = require('log4js')
  .getLogger('update-personal-pccos');
const { pick } = require('lodash');

async function create(ctx, next) {
  const { store: { Pccos, PccosUpdatesLogs }, state: { user }, request: { body, personalPcco } } = ctx;
  let trx;
  try {
    trx = await transaction.start(Pccos);
    await personalPcco.$query(trx)
      .update(pick(body, Pccos.allowedToModificationFields));
    await PccosUpdatesLogs.query(trx)
      .insert({
        affectedRecordId: personalPcco.id,
        type: UPDATE_PCCO,
        updationReason: body.updateReason,
        newValues: body,
        oldValues: pick(personalPcco, Object.keys(body)),
        userId: user.id
      });
    await trx.commit();
    ctx.res.ok();
  } catch (e) {
    logger.error(e);
    await trx.rollback();
    return ctx.res.error(e);
  }
  await next();
}

create.schema = {
  body: Joi.object({
    updateReason: Joi.string(),
    judicatureDecisionDate: Joi.date()
      .optional(),
    judicatureDecisionNumber: Joi.string()
      .optional(),
    judicatureName: Joi.string()
      .optional(),
    litigationNumber: Joi.string()
      .max(64)
      .optional(),
    judicatureDecisionApplyingDate: Joi.date()
      .optional(),
    criminalActionType: Joi.string()
      .optional(),
    criminalCodeArticle: Joi.number()
      .min(0)
      .optional(),
    offenceDescription: Joi.string(),
    firstName: Joi.string()
      .trim()
      .min(2)
      .optional(),
    lastName: Joi.string()
      .trim()
      .min(2)
      .optional(),
    surname: Joi.string()
      .trim()
      .min(2)
      .optional(),
    personalCode: Joi.string()
      .length(8)
      .alphanum()
      .optional(),
    workPosition: Joi.string()
      .optional(),
    workPlace: Joi.string()
      .optional(),
    passportSeries: Joi.string()
      .uppercase()
      .length(2)
      .optional(),
    passportCode: Joi.string()
      .length(6)
      .regex(/^\d+$/)
      .optional(),
    dob: Joi.date()
      .optional(),
    placeOfBirth: Joi.string()
      .optional(),
    residence: Joi.string()
      .default('UKR')
      .optional(),
    impositionDisciplinaryActionDetails: Joi.string()
      .optional(),
    offenceMethod: Joi.string()
      .optional(),
    disciplinaryActionCancellationDate: Joi.date()
      .optional(),
    disciplinaryActionCancellationReason: Joi.string()
      .optional(),
    passportIssuingAuthority: Joi.string()
      .optional(),
    convictionRepaymentDate: Joi.date()
      .optional(),
    convictionRepaymentReason: Joi.string()
      .optional(),
    disciplinaryPunishmentType: Joi.string()
      .optional()
  })
};

module.exports = create;
