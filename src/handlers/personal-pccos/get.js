async function get(ctx, next) {
  const { personalPcco } = ctx.request;
  ctx.res.ok({ data: personalPcco.toPrivateJson() });
  await next();
}

module.exports = get;
