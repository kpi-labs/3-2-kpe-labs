const { Joi } = require('../../helpers/JoiObjects/validation-objects');
const _ = require('lodash');
const logger = require('log4js')
  .getLogger('load-personal-pccos');

async function load(ctx, next) {
  const { store: { Pccos } } = ctx;
  try {
    const pcco = await Pccos.query()
      .findById(ctx.request.params.pccoId);
    if (!pcco) {
      return ctx.res.notFound();
    } else {
      ctx.request.personalPcco = pcco;
    }
  } catch (e) {
    logger.error(e);
    return ctx.res.error(e);
  }
  await next();
}

load.schema = {
  params: Joi.object({
    pccoId: Joi.number()
      .min(0),
  }),
};

module.exports = load;
