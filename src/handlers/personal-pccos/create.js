const { Joi } = require('../../helpers/JoiObjects/validation-objects');
const { transaction } = require('objection');
const { pccosUpdatesTypes: { CREATE_PCCO } } = require('../../constants');
const logger = require('log4js')
  .getLogger('create-personal-pccos');

async function create(ctx, next) {
  const { store: { Pccos, PccosUpdatesLogs }, state: { user } } = ctx;
  let trx;
  try {
    trx = await transaction.start(Pccos);
    const pccos = await Pccos.query(trx)
      .insertAndFetch({
        ...ctx.request.body,
        isPersonal: true
      })
      .select('*');
    await PccosUpdatesLogs.query(trx)
      .insert({
        affectedRecordId: pccos.id,
        type: CREATE_PCCO,
        updationReason: 'Додавання інформації про ОВКП',
        newValues: pccos.toPrivateJson(),
        userId: user.id
      });
    await trx.commit();
    ctx.res.ok({ data: pccos.toPrivateJson() });
  } catch (e) {
    if (e.code === '23505') {
      return ctx.res.badRequest({ message: 'duplicates are not allowed' });
    }
    logger.error(e);
    await trx.rollback();
    return ctx.res.error(e);
  }
  await next();
}

create.schema = {
  body: Joi.object({
    judicatureDecisionDate: Joi.date(),
    judicatureDecisionNumber: Joi.string(),
    judicatureName: Joi.string(),
    litigationNumber: Joi.string()
      .max(64),
    judicatureDecisionApplyingDate: Joi.date(),
    criminalActionType: Joi.string(),
    criminalCodeArticle: Joi.number()
      .min(0),
    offenceDescription: Joi.string(),
    firstName: Joi.string()
      .trim()
      .min(2),
    lastName: Joi.string()
      .trim()
      .min(2),
    surname: Joi.string()
      .trim()
      .min(2),
    personalCode: Joi.string()
      .length(8)
      .alphanum(),
    workPosition: Joi.string()
      .optional(),
    workPlace: Joi.string()
      .optional(),
    passportSeries: Joi.string()
      .uppercase()
      .length(2),
    passportCode: Joi.string()
      .length(6)
      .regex(/^\d+$/),
    dob: Joi.date(),
    placeOfBirth: Joi.string(),
    residence: Joi.string()
      .default('UKR')
      .optional(),
    impositionDisciplinaryActionDetails: Joi.string()
      .optional(),
    offenceMethod: Joi.string(),
    disciplinaryActionCancellationDate: Joi.date()
      .optional(),
    disciplinaryActionCancellationReason: Joi.string()
      .optional(),
    passportIssuingAuthority: Joi.string()
      .optional(),
    convictionRepaymentDate: Joi.date()
      .optional(),
    convictionRepaymentReason: Joi.string()
      .optional(),
    disciplinaryPunishmentType: Joi.string()
      .optional()
  })
};

module.exports = create;
