const { Joi } = require('../../helpers/JoiObjects/validation-objects');
const _ = require('lodash');
const logger = require('log4js')
  .getLogger('create-pccos');

const POSSIBLE_QUERIED_COLUMNS = ['judicatureDecisionNumber', 'criminalCodeArticle', 'firstName', 'lastName', 'surname', 'litigationNumber'];

function getWhereQuery(reqQuery, Pccos) {
  let q = Pccos.query()
    .where({
      isActive: true,
      isPersonal: true
    });
  const queryParams = _.pick(reqQuery, POSSIBLE_QUERIED_COLUMNS);
  for (const column in queryParams) {
    if(column==='criminalCodeArticle'){
      q = q.andWhere(column, '=', queryParams[column]);
    }else
    if (typeof queryParams[column] === 'string') {
      q = q.andWhere(column, 'ilike', `${queryParams[column]}%`);
    } else {
      q = q.andWhere(column, '=', queryParams[column]);
    }
  }
  return q;
}

async function list(ctx, next) {
  const { store: { Pccos } } = ctx;
  try {
    const query = getWhereQuery(ctx.request.query, Pccos);
    const [pccos, { total }] = await Promise.all([
      query.clone()
        .offset(ctx.request.query.skip)
        .limit(ctx.request.query.limit)
        .select(Pccos.publicFields),
      query.count('id as total')
        .first()]);
    ctx.res.ok({
      data: pccos.map(r => r.toPublicJson()),
      meta: {
        total: total,
        limit: ctx.request.query.limit,
        skip: ctx.request.query.skip,
      }
    });
  } catch (e) {
    logger.error(e);
    return ctx.res.error(e);
  }
  await next();
}

list.schema = {
  query: Joi.object({
    judicatureDecisionNumber: Joi.string()
      .optional(),
    criminalCodeArticle: Joi.number()
      .min(0)
      .optional(),
    firstName: Joi.string()
      .trim()
      .min(2)
      .optional(),
    lastName: Joi.string()
      .trim()
      .min(2)
      .optional(),
    surname: Joi.string()
      .trim()
      .min(2)
      .optional(),
    litigationNumber: Joi.string()
      .optional(),
    skip: Joi.number()
      .min(0)
      .optional()
      .default(0),
    limit: Joi.number()
      .min(1)
      .optional()
      .default(20)
  })
};

module.exports = list;
