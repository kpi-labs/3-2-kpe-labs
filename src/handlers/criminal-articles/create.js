const { Joi } = require('../../helpers/JoiObjects/validation-objects');

async function create(ctx, next) {
  const { CriminalArticleModel } = ctx.store;
  try {
    const article = await CriminalArticleModel.query()
      .insertAndFetch(ctx.request.body)
      .select('id', 'article');
    return ctx.res.ok({ data: article });
  } catch (e) {
    if (e.code === '23505') {
      return ctx.res.badRequest({ message: 'This article is already exists' });
    }
    throw e;
  }
}

create.schema = {
  body: Joi.object({
    article: Joi.string()
  })
};

module.exports = create;
