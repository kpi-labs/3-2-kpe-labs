const { Joi } = require('../../helpers/JoiObjects/validation-objects');

async function update(ctx, next) {
  const { CriminalArticleModel } = ctx.store;
  await CriminalArticleModel.query()
    .findById(ctx.request.params.id)
    .update(ctx.request.body);
  await next();
}

update.schema = {
  params: Joi.object({
    id: Joi.number()
      .min(0)
  }),
  body: Joi.object({
    article: Joi.string()
  })
};

module.exports = update;
