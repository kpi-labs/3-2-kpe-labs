async function list(ctx) {
  const { CriminalArticleModel } = ctx.store;
  ctx.res.ok({
    data: await CriminalArticleModel.query()
      .select('article', 'id')
  });
}

list.schema = {};

module.exports = list;
