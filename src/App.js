const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const cors = require('koa-cors');
const compress = require('koa-compress');
const helmet = require('koa-helmet');
const koaLogger = require('koa-logger');
const router = require('./routes');
const connections = require('./helpers/connections');
const koaQS = require('koa-qs');
const { Model: ObjectionModel } = require('objection');
const errors = require('./constants/errors');
const { responseHandlers, errorHandler, auth } = require('./middlewares');
const log4js = require('log4js');
const store = require('./store');

const httpLogger = log4js.getLogger('http');

class App extends Koa {
  constructor(config, ...params) {
    super(...params);
    // Trust proxy
    this.proxy = true;
    this.servers = [];
    this._configureContext(config);
    this._configureMiddlewares();
    this._configureRoutes();
  }

  _configureMiddlewares() {
    this.use(compress());
    this.use(helmet());
    this.use(auth.initialize());
    if (this.context.config.isDev) {
      this.use(koaLogger((str) => {
        httpLogger.debug(str);
      }));
    }
    this.use(
      bodyParser({
        enableTypes: ['json'],
        jsonLimit: '1mb'
      })
    );
    this.use(cors({
      origin: '*',
      allowMethods: ['GET', 'HEAD', 'PUT', 'POST', 'DELETE', 'PATCH'],
      allowHeaders: ['Content-Type', 'Authorization'],
      exposeHeaders: ['Content-Length', 'Date', 'X-Request-Id']
    }));
    koaQS(this, 'extended');
    this.use(responseHandlers());
    this.use(errorHandler());
  }

  _configureContext(config) {
    this.context.errors = errors;
    this.context.isDev = config.isDev;
    this.context.ROOT = process.cwd();
    this.context.config = config;
    this.context.knex = connections.getKnex(config.knex);
    this.context.objection = ObjectionModel.knex(this.context.knex);
    this.context.store = store;
  }

  _configureRoutes() {
    // Bootstrap application router
    this.use(router.routes());
    this.use(router.allowedMethods());
  }

  listen(...args) {
    const server = super.listen(...args);
    this.servers.push(server);
    return server;
  }

  async terminate() {
    this.context.knex.destroy();
    for (const server of this.servers) {
      server.close();
    }
  }
}

module.exports = App;
