/**
 * Return middleware that handle exceptions in Koa.
 * Dispose to the first middleware.
 *
 * @return {function} Koa middleware.
 */
function errorHandler() {
  return async (ctx, next) => {
    try {
      await next();
      // Respond 404 Not Found for unhandled request
      if (!ctx.body && (!ctx.status || ctx.status === 404)) {
        ctx.res.error(ctx.errors.UNKNOWN_ENDPOINT);
      }
    } catch (err) {
      if (ctx.isDev) {
        ctx.res.error({
          ...ctx.errors.UNKNOWN_ERROR,
          data: err
        });
      } else {
        ctx.res.error(ctx.errors.UNKNOWN_ERROR);
      }
      // Recommended for centralized error reporting,
      // retaining the default behaviour in Koa
      ctx.app.emit('error', err, ctx);
    }
  };
}

module.exports = errorHandler;
