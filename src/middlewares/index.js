module.exports = {
  errorHandler: require('./errorHandler'),
  responseHandlers: require('./responseHandlers'),
  auth: require('./auth'),
  ok
};

function ok(ctx) {
  return ctx.res.ok();
}
