const passport = require('koa-passport');
const config = require('../../config/server');
const { UserBasicStrategy, UserJWTStrategy } = require('../helpers/passport');

passport.use('local', UserBasicStrategy);
passport.use('jwt-access', UserJWTStrategy.create(config.jwt.access.secret));
passport.use('jwt-refresh', UserJWTStrategy.create(config.jwt.refresh.secret));

module.exports.authenticate = (strategies, opts) => async (ctx, next) => {
  await passport.authenticate(strategies, {
    ...opts,
    session: false
  }, async (err, user) => {
    if (!user) {
      ctx.res.unauthorized({ message: err });
    } else {
      ctx.state.user = user;
      await next();
    }
  })(ctx, next);
};

module.exports.initialize = (opts) => passport.initialize(opts);
