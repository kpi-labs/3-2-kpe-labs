const jwt = require('./jwt');


const getTokenForUser = async (user, jwtTokenConfig) => ({
  token: await jwt.sign({ id: user.id }, jwtTokenConfig.secret, {
    expiresIn: jwtTokenConfig.ttl// Math.floor(Date.now() / 1000) +
  }),
  expiresAt: Date.now() + jwtTokenConfig.ttl * 1000
});

const getTokensForUser = async (user, jwtConfig) => {
  const [access, refresh] = await Promise.all([
    getTokenForUser(user, jwtConfig.access),
    getTokenForUser(user, jwtConfig.refresh),
  ]);
  return {
    access,
    refresh
  };
};


module.exports = {
  getTokenForUser,
  getTokensForUser
};
