const validationRules = require('../../constants/validations');

function PasswordExtension(joi) {
  return {
    base: joi.string(),
    name: 'password',
    language: {
      noUpperCase: 'missing uppercase letter',
      noLowerCase: 'missing lowercase letter',
      noDigits: 'missing digits',
      noLetters: 'missing letters',
      noSpecial: 'missing special symbols',
      invalidDifficultPassword:
        'must contains at least one upper and lower case letters, one digit and special symbol',
      invalidPassword:
        'must contains at least one letter and one digit'
    },
    rules: [
      {
        name: 'isDifficultPassword',
        validate(params, value, state, options) {
          if (!validationRules.user.password.regexp.test(value)) {
            if (!/.*[a-z].*/.test(value)) {
              return this.createError('password.noLowerCase', { value }, state, options);
            }
            if (!/.*[A-Z].*/.test(value)) {
              return this.createError('password.noUpperCase', { value }, state, options);
            }
            if (!/.*\d.*/.test(value)) {
              return this.createError('password.noDigits', { value }, state, options);
            }
            if (!/.*[!@#$%^&*].*/.test(value)) {
              return this.createError('password.noSpecial', { value }, state, options);
            }
            return this.createError('password.invalidDifficultPassword', { value }, state, options);
          }
          return value;
        }
      },
      {
        name: 'isPassword',
        validate(params, value, state, options) {
          if (!validationRules.user.password.regexp.test(value)) {
            if (!/.*[A-Za-z].*/.test(value)) {
              return this.createError('password.noLetters', { value }, state, options);
            }
            if (!/.*\d.*/.test(value)) {
              return this.createError('password.noDigits', { value }, state, options);
            }
            return this.createError('password.invalidPassword', { value }, state, options);
          }
          return value;
        }
      }
    ]
  };
}

module.exports = PasswordExtension;
