const bcrypt = require('bcrypt');

module.exports.encryptPassword = async (password, rounds) => bcrypt.hash(password, rounds);

module.exports.comparePasswords = async (plain, hash) => bcrypt.compare(plain, hash);
