const { BasicStrategy } = require('passport-http');
const passwordsTools = require('../passwords');

const authenticate = async (req, email, password, done) => {
  try {
    const user = await req.ctx.store.Users.findByEmail(email)
      .select('password', 'id');
    if (user && await passwordsTools.comparePasswords(password, user.password)) {
      done(null, await req.ctx.store.Users.query()
        .findById(user.id));
    } else {
      done('Invalid email or password', false);
    }
  } catch (e) {
    done(e, false);
  }
};

module.exports = new BasicStrategy({ passReqToCallback: true }, authenticate);
