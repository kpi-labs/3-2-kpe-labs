const Router = require('koa-router');
const { middleware: validation } = require('@zulus/request-validator');
const { authenticate } = require('../middlewares/auth');
const {
  create,
  list,
  update
} = require('../handlers/criminal-articles');

const router = Router();

router.post('/', authenticate('jwt-access'), validation(create.schema), create);
router.get('/', list);
router.put('/:id', authenticate('jwt-access'), validation(update.schema), update);

module.exports = router;
