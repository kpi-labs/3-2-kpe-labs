const Router = require('koa-router');
const ping = require('../handlers/ping');
const authRouter = require('./auth');
const personalPccosRouter = require('./personal-pccos');
const criminalArticlesRouter = require('./criminal-articles');
const pccosUpdatesLogs = require('./pccos-updates-logs');
const usersRouter = require('./users');

const router = Router();
const apiRouter = Router();

apiRouter.get('/ping', ping);
apiRouter.use('/auth', authRouter.routes());
apiRouter.use('/auth', authRouter.allowedMethods());
apiRouter.use('/users', usersRouter.routes());
apiRouter.use('/users', usersRouter.allowedMethods());
apiRouter.use('/personal-pccos', personalPccosRouter.routes());
apiRouter.use('/personal-pccos', personalPccosRouter.allowedMethods());
apiRouter.use('/criminal-articles', criminalArticlesRouter.routes());
apiRouter.use('/criminal-articles', criminalArticlesRouter.allowedMethods());
apiRouter.use('/pccos-updates-logs', pccosUpdatesLogs.routes());
apiRouter.use('/pccos-updates-logs', pccosUpdatesLogs.allowedMethods());

router.use('/api/v1', apiRouter.routes());
router.use('/api/v1', apiRouter.allowedMethods());

module.exports = router;
