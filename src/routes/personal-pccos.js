const Router = require('koa-router');
const { middleware: validation } = require('@zulus/request-validator');
const { authenticate } = require('../middlewares/auth');
const {
  create,
  get,
  load,
  list,
  update
} = require('../handlers/personal-pccos');

const router = Router();

router.post('/', authenticate('jwt-access'), validation(create.schema), create);
router.get('/', validation(list.schema), list);
router.get('/:pccoId', validation(load.schema), load, get);
router.put('/:pccoId', validation(load.schema), validation(update.schema), authenticate('jwt-access'), load, update);

module.exports = router;
