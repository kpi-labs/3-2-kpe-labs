const Router = require('koa-router');
const { middleware: validation } = require('@zulus/request-validator');
const { authenticate } = require('../middlewares/auth');
const {
  get,
  list,
  load
} = require('../handlers/pccos-updates-logs');

const router = Router();

router.get('/', validation(list.schema), list);
router.get('/:logId', validation(load.schema), load, get);

module.exports = router;
