const Router = require('koa-router');
const { middleware: validation } = require('@zulus/request-validator');
const { authenticate } = require('../middlewares/auth');
const {
  create,
  update,
  load,
  get,
  list
} = require('../handlers/users');

const router = Router();

router.post('/', validation(create.schema), create);
router.get('/:userId', validation(load.schema), authenticate('jwt-access'), load, get);
router.get('/', validation(list.schema), authenticate('jwt-access'), list);
router.put('/:userId', validation(load.schema), validation(update.schema), authenticate('jwt-access'), load, update);


module.exports = router;
