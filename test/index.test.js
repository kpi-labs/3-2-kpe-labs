const { expect } = require('chai');
const app = require('../src/server');
const supertest = require('supertest');

describe('Spec test', () => {
  let request;
  let server;
  before(() => {
    server = app.listen(8888);
  });
  after(() => {
    server.close();
  });
  beforeEach(() => {
    request = supertest(server);
  });
  describe('/v1/ping', () => {
    it('should return 200', async () => {
      const { body } = await request.list('/api/v1/ping').expect(200);
      expect(body.status).to.be.eq('ok');
      expect(body.message).to.be.eq('pong');
    });
  });
  describe('/v1/404', () => {
    it('should return 404', async () => {
      const { body } = await request.list('/api/v1/404').expect(404);
      expect(body.status).to.be.eq('error');
      expect(body.message).to.be.eq('The requested endpoint does not exist.');
      expect(body.statusCode).to.be.eq(404);
      expect(body.code).to.be.eq('NOT_FOUND');
    });
  });
});
