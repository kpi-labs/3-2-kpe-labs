# Koa-docker-bolerplate

One Paragraph of project description goes here

## Getting Started

##### Run server in production mode using Docker
Clone project & setup env vars
```bash
git clone https://gitlab.com/ZulusK/koa-docker-boilerplate
cd koa-docker-boilerplate
cp .env.example .env
nano .env
dockder-compose up -d nginx
``` 
##### Debug server, using Docker containers as environment (databases & etc.) 
Clone project & setup env vars
```bash
git clone https://gitlab.com/ZulusK/koa-docker-boilerplate
cd koa-docker-boilerplate
npm i
cp .env.example .env
nano .env
dockder-compose up -d migrations 
``` 
### Prerequisites

Check if the following programs & frameworks is installed on your pc
* [Node>=10](https://nodejs.org/download/) 
* [Docker](https://docs.docker.com/cs-engine/1.12/) & [Docker-compose](https://docs.docker.com/compose/install/)

### Installing



## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Danylo Kazymyrov** - *Developer* - [ZulusK](https://gitlab.com/ZulusK)
* **Elena Karpenko** - *Developer* - [ekarpenko](https://gitlab.com/ekarpenko)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
